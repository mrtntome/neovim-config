return require('packer').startup(function(use)
    use {'wbthomason/packer.nvim'}
    use {
        'catppuccin/nvim',
        as = 'catppuccin',
        config = function() require('config.catppuccin') end
    }
    use {
        'nvim-telescope/telescope.nvim', 
	tag = '0.1.1',
	requires =  {{'nvim-lua/plenary.nvim'}},
        config = function() require('config.telescope') end
    }
    use {
        'tpope/vim-fugitive'
    }
    use {
        'nvim-treesitter/nvim-treesitter',
        run = function()
            local ts_update = require('nvim-treesitter.install').update({with_sync=true})
            ts_update()
        end
    }
end)
